// console.log('hello')

// *Section: Document Object Model (DOM)
    // *allows us to access or modify the properties of an html elements in a webpage
    // *we use the DOM as a standard on how to get, change, add, or delete HTML elements
    // *we will be focusing on the use of DOM in managing forms.
// *For selecting HTML elements we will be using document.querySelector
    // todo Syntax: document.querySelector('html element')
    // *the querySelector function takes a string input that is formatted like a css selector when applying the styles

    const txtFirstName=document.querySelector('#txt-first-name')
    const txtLastName=document.querySelector('#txt-last-name')
    // console.log(txtFirstName);
    const black=document.querySelector('.black')
    const red=document.querySelector('.red')
    const blue=document.querySelector('.blue')
    const green=document.querySelector('.green')

    const name=document.querySelectorAll('.full-name')
    // console.log(name)

    const span=document.querySelectorAll('span')
    // console.log(span)

    const text=document.querySelectorAll('input[type]')

    const spanFullName = document.querySelector("#fullName")
    // console.log(text)

    // const div=document.querySelectorAll('.first-div > .second-span')
    // console.log(div)

// *Event listeners
    // *whenever a user interacts with a webpage, this action is considered as an event
    // *working with events is a large part of creating interactivity in a webpage
    // *based on the events, specific function that will perform an action
// *To do that, the function that will be using is "addEventListener", it takes two arguments
    // *first argument a string identifying an event
    // *second argument, function that the listener will trigger once the "specified event" is triggered

    txtFirstName.addEventListener('keyup', (event) => {
        spanFullName.innerHTML = txtFirstName.value;
        // console.log(event.target.value)
        console.log(event,target);
        console.log(event.target.value);
    })

    const fullName=()=>{
        spanFullName.textContent=`${txtFirstName.value} ${txtLastName.value}`
    }

    const changeColorBlack=()=>{
        spanFullName.style.color=`black`
        console.log(black.value)
    }
    const changeColorBlue=()=>{
        spanFullName.style.color=`blue`
        console.log(blue.value)
    }
    const changeColorRed=()=>{
        spanFullName.style.color=`red`
        console.log(red.value)
    }
    const changeColorGreen=()=>{
        spanFullName.style.color=`green`
        console.log(green.value)
    }

    txtFirstName.addEventListener('keyup',fullName)
    txtLastName.addEventListener('keyup',fullName)
    black.addEventListener('click',changeColorBlack)
    blue.addEventListener('click',changeColorBlue)
    red.addEventListener('click',changeColorRed)
    green.addEventListener('click',changeColorGreen)
